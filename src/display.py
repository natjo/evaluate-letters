import operator
from pprint import pprint
import matplotlib.pyplot as plt
import seaborn as sns


def plot_2D(data):
    sns.set(color_codes=True)
    sorted_data = sorted(data, key=lambda x: x["mentions_I"])
    pprint(sorted_data)
    x = [x['mentions_I'] for x in sorted_data]
    y = [x['total_rating'] for x in sorted_data]
    print(x)
    print(y)
    sns.lmplot(x, y)
    plt.show()
