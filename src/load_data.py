import progressbar
from glob import glob
import os.path as p
import sqlite3
import io
import pandas as pd

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage


def generate_dataframe(directory, documents_dir_name="documents"):
    base_data = _get_letter_text(directory, documents_dir_name)
    df = pd.DataFrame(index=base_data['name'], columns=[
                      'text'], data=base_data['text'])
    _update_dataframe_ratings(directory, df)
    return df


def load_dataframe(directory, name="store.csv"):
    path = p.join(directory, name)
    print("Load data from file '{}'".format(path))
    try:
        df = pd.read_pickle(path)
    except FileNotFoundError as e:
        print("FileNotFoundError: {} ({})".format(e.strerror, path))
        df = pd.DataFrame()

    return df


def save_dataframe(df, directory, name="store.csv"):
    path = p.join(directory, name)
    df.to_pickle(path)

def _get_letter_text(directory, documents_dir_name):
    """Read in all text as string from the motivational letters,
       it expects a subdirectory for each applicant in the specified
       directory, which has to contain the motivational letter as a file matching the "[Mm]otiv*.pdf" glob."""
    if not p.exists(directory):
        print("Directory '{}' not found.".format(directory))
        return None

    letters = glob(p.join(directory, documents_dir_name, "*", "[Mm]otiv*.pdf"))

    letter_infos = {'name': [], 'text': []}
    with progressbar.ProgressBar(max_value=len(letters)) as bar:
        cnt = 0
        for path in letters:
            name = p.basename(p.normpath(p.dirname(path)))
            text = _convert_pdf_to_txt(path)
            letter_infos['name'].append(name)
            letter_infos['text'].append(text)
            cnt += 1
            bar.update(cnt)

    return letter_infos


def _update_dataframe_ratings(directory, df):
    """
    Read ratings from database
    """
    db_path = p.join(directory, "imlr_database.db")
    # Open database read only
    db = sqlite3.connect("file:{}?mode=ro".format(db_path), uri=True)
    stmt = """
    SELECT A.applicant_name, SUM(R.rating_style), SUM(R.rating_content), SUM(R.rating_social_skills), SUM(R.rating_creativity), SUM(R.rating_general)
    FROM reviews as R, applications as A
    WHERE R.application = A.id
    GROUP BY R.application
    """
    ratings = db.execute(stmt).fetchall()

    # Write ratings into dataframe
    for rating in ratings:
        df.at[rating[0], 'rating0'] = rating[1]
        df.at[rating[0], 'rating1'] = rating[2]
        df.at[rating[0], 'rating2'] = rating[3]
        df.at[rating[0], 'rating3'] = rating[4]
        df.at[rating[0], 'rating4'] = rating[5]
        df.at[rating[0], 'total_rating'] = sum(rating[1:])
    return df


def _convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages,
                                  password=password,
                                  caching=caching,
                                  check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text
