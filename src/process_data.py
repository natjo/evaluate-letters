import re
import operator
from functools import reduce


def count_mentions_I(data):
    result = _base_processing(data, _count_mentions_I_i)
    return result


def _count_mentions_I_i(text):
    # These patterns catch all I and ich not surrounded by other letters
    patterns = ("[^<a-zA-Z>]I[^<a-zA-Z>]", "[^<a-zA-Z>]ich[^<a-zA-Z>]")

    cnt = 0
    for pattern in patterns:
        result = re.finditer(pattern, text, flags=re.IGNORECASE)
        cnt += reduce(operator.add, (1 for _ in result), 0)

    return cnt


def _base_processing(data, process_fun):
    result = []
    for letter_info in data:
        count = process_fun(letter_info.get_text())
        print("{}: {}".format(letter_info.get_name(), count))
        result.append(
            {"mentions_I": count, "total_rating": letter_info.total_rating})

    return result
