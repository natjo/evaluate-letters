import load_data
import process_data as pda
import os
import display
import my_excepthook
import pandas as pd


def main():
    # Prepare the proper directory path for all data
    path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(path, "../data")
    # df = load_data.generate_dataframe(path, "documents_all")
    df = load_data.load_dataframe(path)
    print(df.head())
    # load_data.save_dataframe(df, path)


if __name__ == '__main__':
    main()
